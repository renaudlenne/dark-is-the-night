# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :dark_is_the_night, DarkIsTheNightWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "y+wdRm6DvbNbucKmaQ8O/LGw6GofynAHXoTg7Bm/LRSvTeToR00NarbSItHgUQLt",
  render_errors: [view: DarkIsTheNightWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: DarkIsTheNight.PubSub,
  live_view: [signing_salt: "5gJKPwDG6P9xxmXlBEC7I0mXouoQhffp"]

config :dark_is_the_night,
  admin_username: "admin",
  admin_password: "admin"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
