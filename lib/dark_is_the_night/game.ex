defmodule DarkIsTheNight.Game do
  @moduledoc """
  Represents the game structure and exposes actions that can be taken to update
  it
  """

  alias DarkIsTheNight.GameCode
  alias DarkIsTheNight.Player

  require Logger

  @type token_type :: :bolt | :feint | :trap
  @type action_type :: :set_initial_position | :attack | :bolt_attack | :bolt_fire | :feint | :trap | :none | :bait | :take_bait

  @type token :: %{
    type: token_type(),
    amount: integer()
  }

  @type action :: %{
    turn: integer(),
    role: role(),
    move: integer(),
    action_type: action_type(),
    action_position: integer() | nil,
  }

  defstruct code: nil,
            players: [],
            online_players: [],
            hunter_player_id: nil,
            monster_player_id: nil,
            hunter_position: nil,
            monster_position: nil,
            hunter_tokens: [
              %{type: :bolt, amount: 2},
              %{type: :trap, amount: 1},
              %{type: :bait, amount: 1},
            ],
            monster_tokens: [
              %{type: :feint, amount: 1},
            ],
            bait_position: nil,
            trap_position: nil,
            fire_positions: [],
            bell_ringing_turn: nil,
            actions_log: [],
            winner: nil,
            turn_role: nil,
            turn: 0

  @type t :: %__MODULE__{
          code: GameCode.t(),
          players: list(Player.t()),
          hunter_player_id: String.t() | nil,
          monster_player_id: String.t() | nil,
          hunter_position: integer() | nil,
          monster_position: integer() | nil,
          hunter_tokens: list(token()),
          monster_tokens: list(token()),
          bait_position: integer() | nil,
          trap_position: integer() | nil,
          fire_positions: list(integer()),
          bell_ringing_turn: integer() | nil,
          actions_log: list(action()),
          winner: role() | nil,
          turn_role: role() | nil,
          turn: integer()
        }

  @role %{
    hunter: "Hunter",
    monster: "Monster"
  }

  @type role :: :hunter | :monster

  @spec new(GameCode.t()) :: t()
  def new(code) do
    struct!(__MODULE__, code: code)
  end

  @spec new() :: t()
  def new do
    struct!(__MODULE__, code: GameCode.new())
  end

  @doc """
  Adds a new player to the game if one with a similar name does not exist
  """
  @spec add_player(t(), Player.t()) :: {:ok, t()} | {:error, :name_taken}
  def add_player(game, player) do
    %{name: name} = player

    case find_player(game, %{name: {:case_insensitive, name}}, match: :any) do
      nil ->
        {:ok, Map.update!(game, :players, &[player | &1])}

      %Player{} ->
        {:error, :name_taken}
    end
  end

  @spec get_player_by_id(t(), String.t()) :: {:ok, Player.t()} | {:error, :player_not_found}
  def get_player_by_id(game, player_id) do
    case find_player(game, %{id: player_id}) do
      %Player{} = player -> {:ok, player}
      nil -> {:error, :player_not_found}
    end
  end

  @doc """
  Updates a player's data
  """
  @spec update_player(t(), Player.t()) :: {:ok, t()} | {:error, :player_not_found}
  def update_player(game, player) do
    case get_player_by_id(game, player.id) do
      {:ok, _} ->
        game = Map.update!(game, :players, &update_player_by_id(&1, player))
        game = set_player_role(game, player.id, player.role)

        {:ok, game}

      {:error, :player_not_found} = error ->
        error
    end
  end

  @doc """
  Removes a player with a given id
  """
  @spec remove_player(t(), String.t()) :: {:ok, t()}
  def remove_player(game, player_id) do
    case get_player_by_id(game, player_id) do
      {:ok, _} ->
        game = Map.update!(game, :players, &Enum.reject(&1, fn %{id: id} -> id == player_id end))
        if game.hunter_player_id == player_id do
          game = %{game | hunter_player_id: nil}
        end
        if game.monster_player_id == player_id do
          game = %{game | monster_player_id: nil}
        end
        {:ok, game}

      {:error, :player_not_found} ->
        {:ok, game}
    end
  end

  @doc """
  Returns a full list of players in the game
  """
  @spec list_players(t()) :: list(Player.t())
  def list_players(game), do: game.players

  defp find_player(game, %{} = attrs, match: :any) do
    game.players
    |> Enum.find(fn player ->
      Enum.any?(attrs, &has_equal_attribute?(player, &1))
    end)
  end

  defp find_player(game, %{} = attrs) do
    game.players
    |> Enum.find(fn player ->
      Enum.all?(attrs, &has_equal_attribute?(player, &1))
    end)
  end

  defp has_equal_attribute?(%{} = map, {key, {:case_insensitive, value}}) when is_binary(value) do
    String.downcase(Map.get(map, key, "")) == String.downcase(value)
  end

  defp has_equal_attribute?(%{} = map, {key, value}) do
    Map.get(map, key) == value
  end

  defp update_player_by_id(players, player) do
    Enum.map(players, fn %{id: id} = original_player ->
      if id == player.id do
        player
      else
        original_player
      end
    end)
  end

  @doc """
  Set the role of a player
  """
  def set_player_role(game, player_id, role) do
    case role do
      :hunter -> %{game | hunter_player_id: player_id}
      :monster -> %{game | monster_player_id: player_id}
    end
  end

  @doc """
  Set the position of a player
  """
  @spec set_position(t(), role(), integer()) :: {:ok, t()} | {:error, :incorrect_position}
  def set_position(_, _, position) when position < 0 do
    {:error, :incorrect_position}
  end

  def set_position(_, :hunter, position) when position > 7 do
    {:error, :incorrect_position}
  end

  def set_position(game, :hunter, position) do
    {:ok, %{game | hunter_position: position}}
  end

  def set_position(_, :monster, position) when position > 11 do
    {:error, :incorrect_position}
  end

  def set_position(%{trap_position: nil} = game, :monster, position) do
    {:ok, %{game | monster_position: position}}
  end

  def set_position(%{bell_ringing_turn: nil} = game, :monster, position) when game.trap_position  == game.monster_position do
    {:ok, %{game | monster_position: position, bell_ringing_turn: game.turn}}
  end

  def set_position(game, :monster, position) do
    {:ok, %{game | monster_position: position}}
  end

  def use_token(game, :monster, token_type) do
    %{
      game |
      monster_tokens: Enum.map(
        game.monster_tokens,
        fn token -> if token.type == token_type, do: %{type: token_type, amount: token.amount - 1}, else: token end
      )
    }
  end

  def use_token(game, :hunter, token_type) do
    %{
      game |
      hunter_tokens: Enum.map(
        game.hunter_tokens,
        fn token -> if token.type == token_type, do: %{type: token_type, amount: token.amount - 1}, else: token end
      )
    }
  end

  def set_bait(game, position) do
    %{use_token(game, :hunter, :bait) | bait_position: position}
  end

  def set_trap(game, position) do
    %{use_token(game, :hunter, :trap) | trap_position: position}
  end

  def set_fire(game, position) do
    %{use_token(game, :hunter, :bolt) | fire_positions: [position | game.fire_positions]}
  end

  @spec next_turn(t(), action()) :: t()
  def next_turn(game, %{role: :hunter, action_type: :set_initial_position} = action) do
    %{game | turn: 1, turn_role: :hunter, actions_log: [action | game.actions_log]}
  end

  def next_turn(game, %{action_type: :set_initial_position} = action) do
    %{game | actions_log: [action | game.actions_log]}
  end

  def next_turn(game, %{action_type: :take_bait} = action) do
    %{game | actions_log: [action | game.actions_log]}
  end

  def next_turn(%{turn_role: :hunter} = game, action) do
    %{game | turn: game.turn+1, turn_role: :monster, actions_log: [action | game.actions_log]}
  end

  def next_turn(game, action) do
    %{game | turn: game.turn+1, turn_role: :hunter, bait_position: nil, actions_log: [action | game.actions_log]}
  end

  def set_winner(game, winner, last_action) do
    %{game | winner: winner, turn_role: nil, actions_log: [last_action | game.actions_log]}
  end

end
