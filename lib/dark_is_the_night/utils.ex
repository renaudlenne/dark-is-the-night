defmodule DarkIsTheNight.Utils do
  @moduledoc """
  Some utilities functions
  """

  require Logger

  @doc """
  Minimum distance between two position in the darkness
  """
  @spec min_darkness_distance(position_1 :: integer(), position_2 :: integer()) :: integer()
  def min_darkness_distance(position_1, position_2) do
    min(clockwise_darkness_distance(position_1, position_2), clockwise_darkness_distance(position_2, position_1))
  end

  @doc """
  Clockwise distance between two position in the darkness
  """
  @spec clockwise_darkness_distance(position_1 :: integer(), position_2 :: integer()) :: integer()
  def clockwise_darkness_distance(position_1, position_2) do
    rem(12+position_1-position_2, 12)
  end

  @doc """
  Is monster_position adjacent to hunter_position
  """
  @spec are_adjacents?(monster_position :: integer(), hunter_position :: integer()) :: bool
  def are_adjacents?(0, 0) do true end
  def are_adjacents?(11, 0) do true end
  def are_adjacents?(1, 1) do true end
  def are_adjacents?(2, 2) do true end
  def are_adjacents?(3, 2) do true end
  def are_adjacents?(4, 3) do true end
  def are_adjacents?(5, 4) do true end
  def are_adjacents?(6, 4) do true end
  def are_adjacents?(7, 5) do true end
  def are_adjacents?(8, 6) do true end
  def are_adjacents?(9, 6) do true end
  def are_adjacents?(10, 7) do true end
  def are_adjacents?(_, _) do false end

  @doc """
  Are hunter_position and monster_position on the same line
  """
  @spec same_line?(hunter_position :: integer(), monster_position :: integer()) :: bool
  def same_line?(hunter_position, _) when hunter_position < 0 do false end
  def same_line?(hunter_position, 3) when hunter_position < 3 do true end
  def same_line?(hunter_position, 11) when hunter_position < 3 do true end
  def same_line?(hunter_position, 4) when hunter_position in [3,7] do true end
  def same_line?(hunter_position, 10) when hunter_position in [3,7] do true end
  def same_line?(hunter_position, 5) when hunter_position in 4..6 do true end
  def same_line?(hunter_position, 9) when hunter_position in 4..6 do true end
  def same_line?(_, _) do false end

  @doc """
  Are hunter_position and monster_position on the same column
  """
  @spec same_column?(hunter_position :: integer(), monster_position :: integer()) :: bool
  def same_column?(hunter_position, 0) when hunter_position in [0,6,7] do true end
  def same_column?(hunter_position, 8) when hunter_position in [0,6,7] do true end
  def same_column?(hunter_position, 1) when hunter_position in [1,5] do true end
  def same_column?(hunter_position, 7) when hunter_position in [1,5] do true end
  def same_column?(hunter_position, 2) when hunter_position in 2..4 do true end
  def same_column?(hunter_position, 6) when hunter_position in 2..4 do true end
  def same_column?(_, _) do false end

  @doc """
  Can a bolt be fired from hunter_position to monster_position
  """
  @spec can_fire_bolt?(hunter_position :: integer(), monster_position :: integer(), bolt_type :: :attack | :fire) :: bool
  def can_fire_bolt?(hunter_position, monster_position, :attack) do
    same_line?(hunter_position, monster_position) || same_column?(hunter_position, monster_position)
  end
  def can_fire_bolt?(1, 7, :fire) do true end
  def can_fire_bolt?(3, 10, :fire) do true end
  def can_fire_bolt?(5, 1, :fire) do true end
  def can_fire_bolt?(7, 4, :fire) do true end
  def can_fire_bolt?(_, _, :fire) do false end

  @doc """
  Is hunter_position next to the campfire
  """
  @spec is_next_to_campfire?(hunter_position :: integer()) :: bool
  def is_next_to_campfire?(hunter_position) when hunter_position < 0 do false end
  def is_next_to_campfire?(hunter_position) when hunter_position > 7 do false end
  def is_next_to_campfire?(hunter_position) when rem(hunter_position, 2) == 1 do true end
  def is_next_to_campfire?(_) do false end

  @doc """
  Returns the darkness tile adjacent to the hunter_position which is closest to monster_position
  """
  @spec closest_darkness(hunter_position :: integer(), monster_position :: integer()) :: integer()
  def closest_darkness(0, monster_position) do
    if min_darkness_distance(monster_position, 0) < min_darkness_distance(monster_position, 11), do: 0, else: 11
  end
  def closest_darkness(0, _) do 11 end
  def closest_darkness(1, _) do 1 end
  def closest_darkness(2, monster_position) do
    if min_darkness_distance(monster_position, 2) < min_darkness_distance(monster_position, 3), do: 2, else: 3
  end
  def closest_darkness(3, _) do 4 end
  def closest_darkness(4, monster_position) do
    if min_darkness_distance(monster_position, 5) < min_darkness_distance(monster_position, 6), do: 5, else: 6
  end
  def closest_darkness(5, _) do 7 end
  def closest_darkness(6, monster_position) do
    if min_darkness_distance(monster_position, 8) < min_darkness_distance(monster_position, 9), do: 8, else: 9
  end
  def closest_darkness(7, _) do 10 end

  def quadrant(position) do
    case position do
      p when p < 3 -> :north
      p when p < 6 -> :east
      p when p < 9 -> :south
      _ -> :west
    end
  end

end
