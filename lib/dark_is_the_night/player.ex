defmodule DarkIsTheNight.Player do
  @moduledoc """
  Represents a player
  """

  alias DarkIsTheNight.Game

  defstruct [
    :id,
    :name,
    :role,
  ]

  @type t :: %__MODULE__{
          id: String.t(),
          name: String.t(),
          role: Game.role() | nil,
        }

  @spec new(String.t()) :: t()
  def new(name) do
    params = %{
      id: UUID.uuid4(),
      name: name
    }

    struct!(__MODULE__, params)
  end

  @spec set_name(t(), String.t()) :: t()
  def set_name(%__MODULE__{} = player, name) do
    name = String.trim(name)

    if name == "" do
      player
    else
      Map.put(player, :name, name)
    end
  end

  @spec set_role(t(), Game.role()) :: t()
  def set_role(%__MODULE__{} = player, role) do
    Map.put(player, :role, role)
  end

end
