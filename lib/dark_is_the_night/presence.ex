defmodule DarkIsTheNightWeb.Presence do
  @moduledoc false

  use Phoenix.Presence, otp_app: :dark_is_the_night, pubsub_server: DarkIsTheNight.PubSub

  alias DarkIsTheNight.GameServer

  def fetch(topic, presences) do
    {:ok, players} = GameServer.list_players(topic)

    players
    |> Enum.reduce(presences, fn %{id: id} = player, acc ->
      case acc[id] do
        %{metas: _} ->
          put_in(acc, [id, :player], player)

        _ ->
          acc
      end
    end)
  end
end
