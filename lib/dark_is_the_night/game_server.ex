defmodule DarkIsTheNight.GameServer do
  @moduledoc """
  Holds state for a game and exposes an interface to managing the game instance
  """

  use GenServer

  alias DarkIsTheNight.Game
  alias DarkIsTheNight.GameCode
  alias DarkIsTheNight.Player

  require Logger

  @spec add_player(String.t(), Player.t()) ::
          :ok | {:error, :game_not_found | :name_taken}
  def add_player(game_id, player) do
    with {:ok, players, hunter_player_id, monster_player_id} <- call_by_name(game_id, {:add_player, player}) do
      broadcast_players_updated!(game_id, players, hunter_player_id, monster_player_id)
      :telemetry.execute([:dark_is_the_night, :player_joined], %{count: 1})
    end
  end

  @spec get_player_by_id(String.t(), String.t()) ::
          {:ok, Player.t()} | {:error, :game_not_found | :player_not_found}
  def get_player_by_id(game_id, player_id) do
    call_by_name(game_id, {:get_player_by_id, player_id})
  end

  @spec update_player(String.t(), Player.t()) ::
          :ok | {:error, :game_not_found | :player_not_found}
  def update_player(game_id, player) do
    with {:ok, players, hunter_player_id, monster_player_id} <- call_by_name(game_id, {:update_player, player}) do
      broadcast_players_updated!(game_id, players, hunter_player_id, monster_player_id)
    end
  end

  @spec process_turn(String.t(), Game.action()) :: :ok | {:error, :incorrect_position}
  def process_turn(game_id, %{role: role, move: position, action_type: :attack, action_position: attack} = action) do
    with {:ok, game} <- call_by_name(game_id, {:move_character, %{position: position, role: role}}) do
      if (role == :monster && attack == game.hunter_position) || (role == :hunter && attack == game.monster_position) do
        game = call_by_name(game_id, {:end_game, %{winner: role, action: action}})
        broadcast_game_ended!(game_id, game)
      else
        game = call_by_name(game_id, {:next_turn, action})
        broadcast_turn_ended!(game_id, game)
      end
    end
  end

  def process_turn(game_id, %{role: role, move: position, action_type: :trap, action_position: trap_position} = action) do
    with {:ok, _} <- call_by_name(game_id, {:move_character, %{position: position, role: role}}) do
      call_by_name(game_id, {:set_trap, %{trap_position: trap_position}})
      game = call_by_name(game_id, {:next_turn, action})
      broadcast_turn_ended!(game_id, game)
    end
  end

  def process_turn(game_id, %{role: role, move: position, action_type: :bait, action_position: bait_position} = action) do
    with {:ok, _} <- call_by_name(game_id, {:move_character, %{position: position, role: role}}) do
      call_by_name(game_id, {:set_bait, %{bait_position: bait_position}})
      game = call_by_name(game_id, {:next_turn, action})
      broadcast_turn_ended!(game_id, game)
    end
  end

  def process_turn(game_id, %{role: role, move: position, action_type: :bolt_attack, action_position: bolt_position} = action) do
    with {:ok, game} <- call_by_name(game_id, {:move_character, %{position: position, role: role}}) do
      game = call_by_name(game_id, {:use_token, %{role: role, token_type: :bolt_attack}})
      if bolt_position == game.monster_position do
        game = call_by_name(game_id, {:end_game, %{winner: role, action: action}})
        broadcast_game_ended!(game_id, game)
      else
        game = call_by_name(game_id, {:next_turn, action})
        broadcast_turn_ended!(game_id, game)
      end
    end
  end

  def process_turn(game_id, %{role: role, move: position, action_type: :bolt_fire, action_position: bolt_position} = action) do
    with {:ok, _} <- call_by_name(game_id, {:move_character, %{position: position, role: role}}) do
      call_by_name(game_id, {:set_fire, %{fire_position: bolt_position}})
      game = call_by_name(game_id, {:next_turn, action})
      broadcast_turn_ended!(game_id, game)
    end
  end

  def process_turn(game_id, %{role: role, move: position, action_type: token_type} = action) do
    with {:ok, _} <- call_by_name(game_id, {:move_character, %{position: position, role: role}}) do
      game = call_by_name(game_id, {:use_token, %{role: role, token_type: token_type}})
      game = call_by_name(game_id, {:next_turn, action})
      broadcast_turn_ended!(game_id, game)
    end
  end

  def remove_player(game_id, player_id) do
    with {:ok, players, hunter_player_id, monster_player_id} <- call_by_name(game_id, {:remove_player, player_id}) do
      broadcast_players_updated!(game_id, players, hunter_player_id, monster_player_id)
    end
  end

  def list_players(game_id) do
    call_by_name(game_id, :list_players)
  end

  @spec get_game(String.t() | pid()) :: {:ok, Game.t()} | {:error, :game_not_found}
  def get_game(pid) when is_pid(pid) do
    GenServer.call(pid, :get_game)
  end

  def get_game(game_id) do
    call_by_name(game_id, :get_game)
  end

  @spec presence_player_ids(String.t()) :: list(String.t())
  def presence_player_ids(game_id) do
    game_id
    |> DarkIsTheNightWeb.Presence.list()
    |> Enum.map(fn {player_id, _} -> player_id end)
  end

  def start_link(%GameCode{} = code) do
    GenServer.start(__MODULE__, code, name: via_tuple(code.game_id))
  end

  def game_pid(game_id) do
    game_id
    |> via_tuple()
    |> GenServer.whereis()
  end

  @impl GenServer
  def init(%GameCode{} = code) do
    Logger.info("Creating game server for #{code.game_code} (#{code.game_id})")
    {:ok, %{game: Game.new(code)}}
  end

  @impl GenServer
  def handle_call({:add_player, player}, _from, state) do
    case Game.add_player(state.game, player) do
      {:ok, game} ->
        {:reply, {:ok, game.players, game.hunter_player_id, game.monster_player_id}, %{state | game: game}}

      {:error, :name_taken} = error ->
        {:reply, error, state}

      {:error, :character_taken} = error ->
        {:reply, error, state}
    end
  end

  @impl GenServer
  def handle_call({:get_player_by_id, player_id}, _from, state) do
    {:reply, Game.get_player_by_id(state.game, player_id), state}
  end

  @impl GenServer
  def handle_call({:update_player, player}, _from, state) do
    case Game.update_player(state.game, player) do
      {:ok, game} ->
        {:reply, {:ok, game.players, game.hunter_player_id, game.monster_player_id}, %{state | game: game}}

      {:error, :player_not_found} = error ->
        {:reply, error, state}
    end
  end

  @impl GenServer
  def handle_call({:remove_player, player_id}, _from, state) do
    {:ok, game} = Game.remove_player(state.game, player_id)
    {:reply, {:ok, game.players, game.hunter_player_id, game.monster_player_id}, %{state | game: game}}
  end

  @impl GenServer
  def handle_call(:list_players, _from, state) do
    {:reply, {:ok, Game.list_players(state.game)}, state}
  end

  @impl GenServer
  def handle_call(:get_game, _from, state) do
    {:reply, {:ok, state.game}, state}
  end

  @impl GenServer
  def handle_call({:move_character, %{position: position, role: role}}, _from, state) do
    case Game.set_position(state.game, role, position) do
      {:ok, game} ->
        {:reply, {:ok, game}, %{state | game: game}}

      {:error, :incorrect_position} = error ->
        {:reply, error, state}
    end
  end

  @impl GenServer
  def handle_call({:next_turn, action}, _from, state) do
    game = Game.next_turn(state.game, action)
    {:reply, game, %{state | game: game}}
  end

  @impl GenServer
  def handle_call({:set_trap, %{trap_position: position}}, _from, state) do
    game = Game.set_trap(state.game, position)
    {:reply, game, %{state | game: game}}
  end

  @impl GenServer
  def handle_call({:set_bait, %{bait_position: position}}, _from, state) do
    game = Game.set_bait(state.game, position)
    {:reply, game, %{state | game: game}}
  end

  @impl GenServer
  def handle_call({:set_fire, %{fire_position: position}}, _from, state) do
    game = Game.set_fire(state.game, position)
    Logger.info(inspect game)
    {:reply, game, %{state | game: game}}
  end

  @impl GenServer
  def handle_call({:end_game, %{winner: winner, action: action}}, _from, state) do
    game = Game.set_winner(state.game, winner, action)
    {:reply, game, %{state | game: game}}
  end

  @impl GenServer
  def handle_call({:use_token, %{role: role, token_type: token_type}}, _from, state) do
    game = Game.use_token(state.game, role, token_type)
    {:reply, game, %{state | game: game}}
  end

  @spec broadcast!(String.t(), atom(), map()) :: :ok
  def broadcast!(game_id, event, payload \\ %{}) do
    Phoenix.PubSub.broadcast!(DarkIsTheNight.PubSub, game_id, %{event: event, payload: payload})
  end

  defp call_by_name(game_id, command) do
    case game_pid(game_id) do
      game_pid when is_pid(game_pid) ->
        GenServer.call(game_pid, command)

      nil ->
        {:error, :game_not_found}
    end
  end

  defp cast_by_name(game_id, command) do
    case game_pid(game_id) do
      game_pid when is_pid(game_pid) ->
        GenServer.cast(game_pid, command)

      nil ->
        {:error, :game_not_found}
    end
  end

  defp broadcast_players_updated!(game_id, players, hunter_player_id, monster_player_id) do
    broadcast!(
      game_id,
      :players_updated,
      %{players: players, hunter_player_id: hunter_player_id, monster_player_id: monster_player_id}
    )
  end

  defp broadcast_bait_taken!(game_id, game) do
    broadcast!(game_id, :bait_taken, game)
  end

  defp broadcast_turn_ended!(game_id, game) do
    broadcast!(game_id, :turn_ended, game)
  end

  defp broadcast_game_ended!(game_id, game) do
    broadcast!(game_id, :game_ended, game)
  end

  @spec via_tuple(String.t()) :: {:via, Registry, {DarkIsTheNight.GameRegistry, String.t()}}
  defp via_tuple(game_id) do
    {:via, Registry, {DarkIsTheNight.GameRegistry, game_id}}
  end
end
