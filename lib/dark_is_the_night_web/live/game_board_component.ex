defmodule DarkIsTheNightWeb.GameBoardComponent do
  @moduledoc """
  Displays all players' stats
  """
  require Logger

  use DarkIsTheNightWeb, :live_component

  alias DarkIsTheNight.Utils
  alias DarkIsTheNightWeb.ActionComponent

  def handle_event("choose_action", %{"position" => position_str}, %{assigns: %{action_type: :attack}}=socket) do
    position = String.to_integer(position_str)
    if (socket.assigns.attack == position) do
      {:noreply, assign(socket, %{attack: nil, action_chosen: false })}
    else
      {:noreply, assign(socket, %{attack: position, attack_direction: Utils.quadrant(position), action_chosen: true })}
    end
  end

  def handle_event("choose_action", %{"position" => position_str}, %{assigns: %{action_type: :trap}}=socket) do
    position = String.to_integer(position_str)
    if (socket.assigns.trap_position == position) do
      {:noreply, assign(socket, %{trap_position: nil, action_chosen: false })}
    else
      {:noreply, assign(socket, %{trap_position: position, action_chosen: true })}
    end
  end

  def handle_event("choose_action", %{"position" => position_str}, %{assigns: %{action_type: :bait}}=socket) do
    position = String.to_integer(position_str)
    if (socket.assigns.bait_position == position) do
      {:noreply, assign(socket, %{bait_position: nil, action_chosen: false })}
    else
      {:noreply, assign(socket, %{bait_position: position, action_chosen: true })}
    end
  end

  def handle_event("choose_action", %{"position" => position_str}, %{assigns: %{action_type: :bolt}}=socket) do
    position = String.to_integer(position_str)
    if (socket.assigns.bolt_position == position) do
      {:noreply, assign(socket, %{bolt_position: nil, action_chosen: false })}
    else
      {:noreply, assign(socket, %{bolt_position: position, action_chosen: true })}
    end
  end

  def handle_event("choose_action", %{"position" => position_str}, %{assigns: %{action_type: :feint, game: game}}=socket) do
    position = String.to_integer(position_str)
    if (socket.assigns.monster_position == position) do
      {
        :noreply,
        assign(
          socket,
          %{action_chosen: false, action_type: :attack, monster_position: game.monster_position}
        )
      }
    else
      {:noreply, assign(socket, %{action_chosen: true, monster_position: position })}
    end
  end

  def handle_event("move_hunter", %{"position" => position_str}, %{assigns: %{game: game}} = socket) do
    position = String.to_integer(position_str)
    if game.hunter_position == nil do
      send(self(), {:submit_turn, %{move: position, role: :hunter, action_type: :set_initial_position}})
    end
    next_action_type = if game.turn <= 1, do: :none, else: :attack
    {
      :noreply,
      assign(
        socket,
        %{
          hunter_position: position,
          has_moved: game.hunter_position != nil,
          attack: nil,
          trap_position: nil,
          bait_position: nil,
          bolt_position: nil,
          action_chosen: game.hunter_position != nil && next_action_type == :none,
          action_type: next_action_type
        }
      )
    }
  end

  def handle_event("move_monster", %{"position" => position_str, "baited" => baited_str}, socket) do
    position = String.to_integer(position_str)
    baited = String.to_existing_atom(baited_str)
    if socket.assigns.game.monster_position == nil do
      send(self(), {:submit_turn, %{move: position, role: :monster, action_type: :set_initial_position}})
    end
    if baited do
           send(
             self(),
             {
               :submit_turn,
               %{
                 move: position,
                 role: :monster,
                 action_type: :take_bait,
                 action_position: socket.assigns.game.bait_position
               }
             }
           )
    end
    next_action_type = if socket.assigns.game.turn <= 2, do: :none, else: :attack
    count_has_action = if baited, do: false, else: socket.assigns.game.hunter_position != nil
  {
    :noreply,
    assign(
      socket,
      %{
        monster_position: position,
        action_type: next_action_type,
        has_moved: count_has_action,
        action_chosen: count_has_action
      }
    )
  }
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :hunter}, hunter_position: hunter_position, action_type: :attack, attack: attack}}=socket) do
    send(self(), {:submit_turn, %{move: hunter_position, role: :hunter, action_type: :attack, action_position: attack}})
    {:noreply, assign(socket, %{attack: nil, action_type: :attack, has_moved: false, action_chosen: false})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :hunter}, hunter_position: hunter_position, action_type: :trap, trap_position: trap}}=socket) do
    send(self(), {:submit_turn, %{move: hunter_position, role: :hunter, action_type: :trap, action_position: trap}})
    {:noreply, assign(socket, %{attack: nil, action_type: :attack, has_moved: false})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :hunter}, hunter_position: hunter_position, action_type: :bait, bait_position: bait}}=socket) do
    send(self(), {:submit_turn, %{move: hunter_position, role: :hunter, action_type: :bait, action_position: bait}})
    {:noreply, assign(socket, %{attack: nil, action_type: :attack, has_moved: false})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :hunter}, hunter_position: hunter_position, action_type: :bolt, bolt_position: bolt, bolt_type: :attack}}=socket) do
    send(self(), {:submit_turn, %{move: hunter_position, role: :hunter, action_type: :bolt_attack, action_position: bolt}})
    {:noreply, assign(socket, %{attack: nil, action_type: :attack, has_moved: false, bolt_position: nil})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :hunter}, hunter_position: hunter_position, action_type: :bolt, bolt_position: bolt, bolt_type: :fire}}=socket) do
    send(self(), {:submit_turn, %{move: hunter_position, role: :hunter, action_type: :bolt_fire, action_position: bolt}})
    {:noreply, assign(socket, %{attack: nil, action_type: :attack, has_moved: false, bolt_position: nil})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :hunter}, hunter_position: hunter_position, action_type: :none}}=socket) do
    send(self(), {:submit_turn, %{move: hunter_position, role: :hunter, action_type: :none}})
    {:noreply, assign(socket, %{attack: nil, action_type: :attack, has_moved: false})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :monster}, monster_position: monster_position, action_type: :none}}=socket) do
    send(self(), {:submit_turn, %{move: monster_position, role: :monster, action_type: :none}})
    {:noreply, assign(socket, %{action_type: :attack, has_moved: false})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :monster}, monster_position: monster_position, game: game, action_type: :feint}}=socket) do
    send(self(), {:submit_turn, %{move: monster_position, role: :monster, action_type: :feint, action_position: game.monster_position}})
    {:noreply, assign(socket, %{action_type: :attack, has_moved: false})}
  end

  def handle_event("end_turn", _, %{assigns: %{player: %{role: :monster}, monster_position: monster_position}}=socket) do
    send(self(), {:submit_turn, %{move: monster_position, role: :monster, action_type: :attack, action_position: monster_attack(monster_position)}})
    {:noreply, assign(socket, %{action_type: :attack, has_moved: false})}
  end

  def handle_event("use_token", %{"token_type" => "trap"}, %{assigns: %{player: %{role: :hunter}}} = socket) do
    {:noreply, assign(socket, %{action_type: :trap})}
  end

  def handle_event("use_token", %{"token_type" => "bait"}, %{assigns: %{player: %{role: :hunter}}} = socket) do
    {:noreply, assign(socket, %{action_type: :bait})}
  end

  def handle_event("use_token", %{"token_type" => "bolt"}, %{assigns: %{player: %{role: :hunter}}} = socket) do
    {:noreply, assign(socket, %{action_type: :bolt, bolt_type: :attack})}
  end

  def handle_event("use_token", _, %{assigns: %{player: %{role: :hunter}}} = socket) do
    {:noreply, socket}
  end

  def handle_event("use_token", %{"token_type" => "feint"}, %{assigns: %{player: %{role: :monster}, game: game, action_type: :feint}} = socket) do
    {
      :noreply,
      assign(
        socket,
        %{action_chosen: false, action_type: :attack, monster_position: game.monster_position}
      )
    }
  end

  def handle_event("use_token", %{"token_type" => "feint"}, %{assigns: %{player: %{role: :monster}}} = socket) do
    {:noreply, assign(socket, %{action_type: :feint, action_chosen: false, monster_position: nil})}
  end

  def handle_event("change_bolt_type", %{"bolt_type" => "fire"}, %{assigns: %{player: %{role: :hunter}}} = socket) do
    {:noreply, assign(socket, %{bolt_type: :fire})}
  end

  def handle_event("change_bolt_type", %{"bolt_type" => "attack"}, %{assigns: %{player: %{role: :hunter}}} = socket) do
    {:noreply, assign(socket, %{bolt_type: :attack})}
  end


  @doc """
  Which tile is the monster attacking from the given position
  """
  @spec monster_attack(monster_position :: integer()) :: integer
  def monster_attack(0) do 0 end
  def monster_attack(1) do 1 end
  def monster_attack(2) do 2 end
  def monster_attack(3) do 2 end
  def monster_attack(4) do 3 end
  def monster_attack(5) do 4 end
  def monster_attack(6) do 4 end
  def monster_attack(7) do 5 end
  def monster_attack(8) do 6 end
  def monster_attack(9) do 6 end
  def monster_attack(10) do 7 end
  def monster_attack(11) do 0 end

  def is_move_valid?(%{ player: %{ role: :monster}, monster_position: monster_position, game: %{turn_role: :monster, bait_position: bait_position} = game} = assigns, :monster, position, true) when monster_position == position do
    bait_position == position
  end

  def is_move_valid?(%{ player: %{ role: :monster}, game: %{turn_role: :monster, bait_position: bait_position} = game} = assigns, :monster, position, true) do
    (Integer.mod(game.monster_position + 1, 12) == position || Integer.mod(game.monster_position - 1, 12) == position) &&
    (Utils.min_darkness_distance(game.monster_position, bait_position) >= Utils.min_darkness_distance(position, bait_position))
  end

  def is_move_valid?(%{ player: %{ role: :monster}, game: game} = assigns, :monster, position, false) do
    assigns.monster_position != position && (
      game.monster_position == nil || (
          game.turn_role == :monster &&
          (Integer.mod(game.monster_position + 1, 12) == position || Integer.mod(game.monster_position - 1, 12) == position)
        )
      )
  end
  def is_move_valid?(_, :monster, _, _) do false end

  def is_move_valid?(assigns, :hunter, position) do
     assigns.player.role == :hunter &&
       assigns.game.monster_position != nil &&
       assigns.hunter_position != position && (
       assigns.game.hunter_position == nil || (
         assigns.game.turn_role == :hunter &&
         (Integer.mod(assigns.game.hunter_position + 1, 8) == position || Integer.mod(assigns.game.hunter_position - 1, 8) == position)
       )
    )
  end


  def can_do_action?(%{game: %{turn: turn}}, _, _) when turn == 1 do false end

  def can_do_action?(%{action_type: :attack} = assigns, :hunter, position) do
    (
      assigns.game.winner == :monster ||
      (assigns.player.role == :monster && assigns.has_moved)
    ) &&
    Utils.are_adjacents?(assigns.monster_position, position)
  end

  def can_do_action?(%{player: %{role: :hunter}, action_type: :attack} = assigns, :monster, position) do
    assigns.has_moved && Utils.are_adjacents?(position, assigns.hunter_position)
  end

  def can_do_action?(%{player: %{role: :hunter}, action_type: :bolt, bolt_type: bolt_type} = assigns, :monster, position) do
    assigns.has_moved && Utils.can_fire_bolt?(assigns.hunter_position, position, bolt_type)
  end

  def can_do_action?(%{player: %{role: :hunter}, action_type: :trap, trap_position: nil} = assigns, :monster, position) do
    assigns.has_moved
  end

  def can_do_action?(%{player: %{role: :hunter}, action_type: :trap, trap_position: tp} = assigns, :monster, position) when tp == position do
    assigns.has_moved
  end

  def can_do_action?(%{player: %{role: :hunter}, action_type: :bait, bait_position: nil} = assigns, :monster, position) do
    assigns.has_moved
  end

  def can_do_action?(%{player: %{role: :hunter}, action_type: :bait, bait_position: bp} = assigns, :monster, position) when bp == position do
    assigns.has_moved
  end

  def can_do_action?(%{player: %{role: :monster}, action_type: :feint, game: %{monster_position: monster_position, hunter_position: hunter_position}} = assigns, :monster, position) do
    if Utils.min_darkness_distance(monster_position, position) >= 3 do
      false
    else
      monster_to_hunter = Utils.min_darkness_distance(monster_position, Utils.closest_darkness(hunter_position, monster_position))
      new_position_to_hunter = Utils.min_darkness_distance(position, Utils.closest_darkness(hunter_position, position))
      monster_to_hunter <= new_position_to_hunter
    end
  end

  def can_do_action?(_, _, _) do false end

  def render_hunter_cell(assigns, position) do
    ~L"""
    <%= with last_action = List.first(@game.actions_log) do %>
      <div
        class="cell light
          <%= if @hunter_position == position, do: "hunter" %>
          <%= if is_move_valid?(assigns, :hunter, position), do: "can_move" %>
          <%= if can_do_action?(assigns, :hunter, position), do: "icon attack #{Utils.quadrant(@monster_position)}" %>
          <%= if last_action && last_action.action_type == :attack && @game.winner == :monster && @hunter_position == position, do: "icon attack #{Utils.quadrant(@monster_position)}" %>
        "
        <%= if is_move_valid?(assigns, :hunter, position) do %>
          phx-click="move_hunter"
          phx-value-position="<%= position %>"
          phx-target="#<%= @id %>"
        <% end %>
        >
      </div>
    <% end %>
    """
  end

  def render_monster_cell(assigns, position) do
    ~L"""
    <%= with previous_action = List.first(@game.actions_log) do %>
      <%= with baited = if previous_action, do: previous_action.action_type == :bait, else: false do %>
        <div
          class="cell darkness
            <%= if baited, do: "baited" %>
            <%= if @monster_position == position && (@player.role == :monster || @game.winner != nil || position in @game.fire_positions), do: "monster" %>
            <%= if @action_type != :feint && is_move_valid?(assigns, :monster, position, baited), do: "can_move" %>
            <%= if @action_type == :feint && can_do_action?(assigns, :monster, position), do: "can_move" %>
            <%= if @action_type != :feint && can_do_action?(assigns, :monster, position), do: "can_do_action" %>
            <%= if @attack == position, do: "icon attack #{@attack_direction}" %>
            <%= if @trap_position == position && !@game.bell_ringing_turn, do: "icon trap" %>
            <%= if @bait_position == position , do: "icon bait" %>
            <%= if @game.bait_position == position , do: "icon bait" %>
            <%= if @bolt_position == position && @bolt_type == :attack, do: "icon bolt #{Utils.quadrant(position)}" %>
            <%= if @bolt_position == position && @bolt_type == :fire, do: "icon fire-bolt #{Utils.quadrant(position)}" %>
            <%= if position in @game.fire_positions, do: "icon fire" %>
            <%= if @game.trap_position == position && @game.bell_ringing_turn && @game.bell_ringing_turn == @game.turn-1, do: "icon trap-ringing" %>
            <%= if previous_action && previous_action.action_type == :attack && @game.winner == :hunter && @monster_position == position, do: "icon attack #{Utils.quadrant(@monster_position)}" %>
            <%= if previous_action && previous_action.action_type == :bolt_fire && previous_action.action_position == position, do: "icon fire-bolt #{Utils.quadrant(position)}" %>
            <%= if previous_action && previous_action.action_type == :bolt_attack && previous_action.action_position == position, do: "icon bolt #{Utils.quadrant(position)}" %>
            <%= if previous_action && previous_action.action_type == :feint && previous_action.action_position == position, do: "icon monster-track" %>
          "
          <%= if @action_type != :feint && is_move_valid?(assigns, :monster, position, baited) do %>
            phx-click="move_monster"
            phx-value-baited="<%= baited %>"
            phx-value-position="<%= position %>"
            phx-target="#<%= @id %>"
          <% end %>
          <%= if can_do_action?(assigns, :monster, position) do %>
            phx-click="choose_action"
            phx-value-position="<%= position %>"
            phx-target="#<%= @id %>"
          <% end %>
          >
        </div>
      <% end %>
    <% end %>
    """
  end

  def instructions(%{game: %{monster_position: nil}, player: %{role: :monster}}) do
    ["Select your initial position"]
  end

  def instructions(%{game: %{monster_position: nil, hunter_position: nil}, player: %{role: :hunter}}) do
    ["Waiting for the monster to select its initial position"]
  end

  def instructions(%{game: %{hunter_position: nil, monster_position: monster_position}, player: %{role: :hunter}}) do
    monster_quadrant = Utils.quadrant(monster_position)
    [
      "Monster initial position is in #{monster_quadrant} Utils.quadrant.",
      "Select your initial position."
    ]
  end

  def instructions(%{player: %{role: :hunter}, game: game}) when game.bell_ringing_turn == game.turn-1 do
    ["The monster just triggered your trap!"]
  end

  def instructions(_) do
    []
  end

  def render(assigns) do
    ~L"""
    <%= with previous_action = List.first(@game.actions_log) do %>
      <%= if @game.turn > 0 do %>
        <div class="turn-counter">
          Turn <%= @game.turn %>
          <span class="role">
            <%= if @game.turn_role == :hunter do %>
              <img src="/images/hooded-assassin.svg" />
            <% else %>
              <img src="/images/werewolf.svg" />
            <% end %>
          </span>
        </div>
      <% end %>
      <%= if @game.winner != nil do %>
        <div class="winner">
          Game ended, <%= @game.winner %> wins!
        </div>
      <% end %>
      <div class="instructions">
        <%= if previous_action != nil do %>
          <%= live_component(@socket, ActionComponent,
            action: previous_action,
            hunter_player: @hunter_player,
            monster_player: @monster_player,
          ) %>
        <% end %>
        <%= for instruction <- instructions(assigns) do %>
          <div>
            <%= instruction %>
          </div>
        <% end %>
      </div>
      <div id="<%= @id %>" class="board">
        <div class="row">
          <div class="cell darkness"></div>
          <%= render_monster_cell(assigns, 0) %>
          <%= render_monster_cell(assigns, 1) %>
          <%= render_monster_cell(assigns, 2) %>
          <div class="cell darkness"></div>
        </div>
        <div class="row">
          <%= render_monster_cell(assigns, 11) %>
          <%= render_hunter_cell(assigns, 0) %>
          <%= render_hunter_cell(assigns, 1) %>
          <%= render_hunter_cell(assigns, 2) %>
          <%= render_monster_cell(assigns, 3) %>
        </div>
        <div class="row">
          <%= render_monster_cell(assigns, 10) %>
          <%= render_hunter_cell(assigns, 7) %>
          <div class="cell light camp"></div>
          <%= render_hunter_cell(assigns, 3) %>
          <%= render_monster_cell(assigns, 4) %>
        </div>
        <div class="row">
          <%= render_monster_cell(assigns, 9) %>
          <%= render_hunter_cell(assigns, 6) %>
          <%= render_hunter_cell(assigns, 5) %>
          <%= render_hunter_cell(assigns, 4) %>
          <%= render_monster_cell(assigns, 5) %>
        </div>
        <div class="row">
          <div class="cell darkness"></div>
          <%= render_monster_cell(assigns, 8) %>
          <%= render_monster_cell(assigns, 7) %>
          <%= render_monster_cell(assigns, 6) %>
          <div class="cell darkness"></div>
        </div>
      </div>
      <%= if @game.turn > 2 do %>
        <div class="tokens">
          <label>Tokens</label>
          <%= if @player.role == :hunter && @game.turn_role == :hunter do %>
            <%= for token <- @game.hunter_tokens do %>
              <button
                <%= if not (@has_moved && token.amount > 0) or @action_chosen, do: "disabled" %>
                phx-click="use_token"
                phx-target="#<%= @id %>"
                phx-value-token_type="<%= token.type %>"
                type="button"
                <%= if @action_type == token.type, do: "class=button-outline" %>
              >
                <%= token.type %> (x<%= token.amount %>)
              </button>
            <% end %>
          <% end %>
          <%= if @player.role == :monster && @game.turn_role == :monster do %>
            <%= for token <- @game.monster_tokens do %>
              <button
                <%= if token.amount == 0, do: "disabled" %>
                phx-click="use_token"
                phx-target="#<%= @id %>"
                phx-value-token_type="<%= token.type %>"
                type="button"
                <%= if @action_type == token.type, do: "class=button-outline" %>
              >
                <%= token.type %> (x<%= token.amount %>)
              </button>
            <% end %>
          <% end %>
        </div>
        <%= if @action_type == :bolt do %>
          <div>
              Bolt type:&nbsp;
              <button
                <%= if @bolt_type == :attack, do: "disabled class=button-outline" %>
                phx-click="change_bolt_type"
                phx-target="#<%= @id %>"
                phx-value-bolt_type="attack"
                type="button"
              >
                Damage
              </button>
              <button
                <%= if @bolt_type == :fire, do: "disabled class=button-outline" %>
                <%= if not Utils.is_next_to_campfire?(@hunter_position), do: "disabled" %>
                phx-click="change_bolt_type"
                phx-target="#<%= @id %>"
                phx-value-bolt_type="fire"
                type="button"
              >
                Fire (light)
              </button>
          </div>
        <% end %>
      <% end %>
      <%= if @game.winner == nil do %>
        <button <%= if @game.turn_role != @player.role || not @action_chosen, do: "disabled" %> phx-click="end_turn" phx-target="#<%= @id %>" type="button">End turn</button>
      <% end %>
    <% end %>
    """
  end

  def mount(socket) do
    {
      :ok,
      assign(
        socket,
        attack: nil,
        bolt_position: nil,
        bolt_type: nil,
        trap_position: nil,
        bait_position: nil,
        has_moved: false,
        action_chosen: false,
        action_type: :set_initial_position
      )
    }
  end
end
