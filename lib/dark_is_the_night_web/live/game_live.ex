defmodule DarkIsTheNightWeb.GameLive do
  @moduledoc """
  LiveView implementation of Dark is the Night
  """
  require Logger

  use DarkIsTheNightWeb, :live_view

  alias DarkIsTheNight.GameServer
  alias DarkIsTheNightWeb.LobbyLive
  alias DarkIsTheNightWeb.GameBoardComponent
  alias DarkIsTheNightWeb.Presence

  def handle_info(
        %{
          event: :players_updated,
          payload: %{
            players: players,
            hunter_player_id: hunter_player_id,
            monster_player_id: monster_player_id
          }
        },
        socket
      ) do
    %{game: game} = socket.assigns

    online_players =
      socket
      |> game_id()
      |> online_players()

    {
      :noreply,
      assign(
        socket,
        game: %{
          game |
          players: players,
          hunter_player_id: hunter_player_id,
          monster_player_id: monster_player_id,
          online_players: online_players
        }
      )
    }
  end

  def handle_info(%{event: :turn_ended, payload: game}, socket) do
    {:noreply, assign(socket, game: game)}
  end

  def handle_info(%{event: :game_ended, payload: game}, socket) do
    {:noreply, assign(socket, game: game)}
  end

  def handle_info(%{event: "presence_diff", payload: _}, socket) do
    %{game: game} = socket.assigns

    online_players =
      socket
      |> game_id()
      |> online_players()

    game = %{game | online_players: online_players}

    {:noreply, assign(socket, game: game)}
  end

  def handle_info({:clear_flash, level}, socket) do
    {:noreply, clear_flash(socket, Atom.to_string(level))}
  end

  @impl true
  def handle_info({:update_player, player}, socket) do
    socket
    |> game_id()
    |> GameServer.update_player(player)
    |> case do
      :ok ->
        {:noreply, assign(socket, player: player)}

      _ ->
        {:noreply, socket}
    end
  end

  def handle_info({:submit_turn, action}, socket) do
    socket
    |> game_id()
    |> GameServer.process_turn(action)
    {:noreply, socket}
  end

  def game_started?(game) do
    game.hunter_player_id != nil and game.monster_player_id != nil
  end

  def monster_player(game) do
    Enum.find(game.players, fn(player) -> player.id == game.monster_player_id end)
  end

  def hunter_player(game) do
    Enum.find(game.players, fn(player) -> player.id == game.hunter_player_id end)
  end

  @impl true
  def render(assigns) do
    ~L"""
    <div id="GameContainer" class="game-container" phx-hook="GameContainer">
      <%= if game_started?(@game) do %>
        <%= live_component(@socket, GameBoardComponent,
          id: "board",
          game: @game,
          player: @player,
          hunter_player: hunter_player(@game),
          monster_player: monster_player(@game),
          hunter_position: @game.hunter_position,
          monster_position: @game.monster_position,
        ) %>
      <% else %>
        <div>
          Waiting for another player...
          <br/>
          <a id="copy-link" href="#" class="button button-secondary">Copy Link</a>
        </div>
      <% end %>
    </div>
    """
  end

  @impl true
  def mount(_params, session, socket) do
    socket =
      with %{"game_id" => game_id, "player_id" => player_id} <- session,
           {:ok, game} <- GameServer.get_game(game_id),
           {:ok, player} <- GameServer.get_player_by_id(game_id, player_id),
           {:ok, _} <- Presence.track(self(), game_id, player_id, %{}),
           :ok <- Phoenix.PubSub.subscribe(DarkIsTheNight.PubSub, game_id) do
        assign(socket, game: game, player: player)
      else
        _ ->
          params =
            if Map.has_key?(session, "game_code") do
              Map.take(session, ["game_code"])
            else
              []
            end

          lobby_path = Routes.live_path(socket, LobbyLive, params)
          push_redirect(socket, to: lobby_path)
      end

    {:ok, socket}
  end

  defp online_players(game_id) do
    game_id
    |> Presence.list()
    |> Enum.map(fn {_k, %{player: player}} -> player end)
  end

  defp game_id(socket) do
    socket.assigns.game.code.game_id
  end
end
