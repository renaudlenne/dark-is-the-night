defmodule DarkIsTheNightWeb.LobbyComponent do
  @moduledoc """
  UI for joining a game room
  """
  require Logger

  use DarkIsTheNightWeb, :live_component

  alias DarkIsTheNight.GameCode

  @type lobby_mode :: :join | :create

  def handle_event("join-game", %{"game_code" => code}, socket) when byte_size(code) < 2 do
    if socket.assigns.mode == :join do
      send(self(), {:put_temporary_flash, :error, "code must be at least 2 characters long"})
    end
    {:noreply, socket}
  end

  def handle_event("join-game", %{"player_name" => name}, socket) when byte_size(name) < 2 do
    send(self(), {:put_temporary_flash, :error, "name must be at least 2 characters long"})
    {:noreply, socket}
  end

  def handle_event("join-game", %{"player_name" => player_name, "role" => role}, socket) when socket.assigns.mode == :create do
    payload = %{
      game_code: GameCode.generate_game_code(),
      role: String.to_existing_atom(role),
      player_name: player_name
    }

    send(self(), {:join_game, payload})

    {:noreply, socket}
  end

  def handle_event("join-game", %{"game_code" => code, "player_name" => player_name}, socket) do
    payload = %{
      game_code: code,
      player_name: player_name
    }

    send(self(), {:join_game, payload})

    {:noreply, socket}
  end

  def handle_event("update", fields, socket) do
    case fields do
      %{"player_name" => player_name} ->
        socket =
          socket
          |> assign(
               player_name: player_name
             )

      %{"game_code" => code} ->
        socket =
          socket
          |> assign(
               game_code: code
             )
    end
    {:noreply, socket}
  end

  def handle_event("switch-to-join-mode", _, socket) do
    {:noreply, assign(socket, mode: :join)}
  end

  def handle_event("switch-to-create-mode", _, socket) do
    {:noreply, assign(socket, mode: :create)}
  end

  def render(assigns) do
    ~L"""
    <div id="<%= @id %>" phx-target="#<%= @id %>" class="lobby-container">
      <%= if @mode == nil do %>
        <button phx-click="switch-to-join-mode" phx-target="#<%= @id %>" type="button">Join a game</button>
        <button phx-click="switch-to-create-mode" phx-target="#<%= @id %>" type="button">Create a new game</button>
      <% end %>

      <%= if @mode != nil do %>
        <form phx-change="update" phx-submit="join-game" phx-target="#<%= @id %>">
          <section class="player-details">
            <label>
              Player Name:
              <input name="player_name" type="text" value="<%= @player_name %>" />
            </label>
          </section>


          <%= if @mode == :join do %>
            <div class="row">
              <div class="column">
                <label>
                  Game Code:
                  <input name="game_code" type="text" value="<%= @game_code %>"/>
                </label>
              </div>
            </div>
          <% end %>

          <%= if @mode == :create do %>
            <div class="row">
              <div class="column">
                <label>
                  Role:
                  <select name="role" onchange="this.blur()">
                    <option value="hunter" <%= if @role == "hunter", do: "selected" %>>Hunter</option>
                    <option value="monster" <%= if @role == "monster", do: "selected" %>>Monster</option>
                  </select>
                </label>
              </div>
            </div>
          <% end %>

          <button type="submit">OK</button>
        </form>
      <% end %>
    </div>
    """
  end

  def update(assigns, socket) do
    mode = if Map.get(assigns, :game_code) != nil, do: :join, else: nil
    {:ok, assign(socket, Map.put(assigns, :mode, mode))}
  end

  def mount(socket) do
    {:ok, assign(socket, player_name: "", role: nil)}
  end
end
