defmodule DarkIsTheNightWeb.ActionComponent do
  @moduledoc """
  Display an action's description
  """
  require Logger

  use DarkIsTheNightWeb, :live_component

  alias DarkIsTheNight.Utils
  alias DarkIsTheNightWeb.PlayerRoleComponent

  def render_action(assigns, %{action_type: :take_bait, action_position: bait_position, move: monster_position} = action) do
    ~L"""
    <%= with distance = Utils.min_darkness_distance(bait_position, monster_position) do %>
      moved closer to the bait.<br/>
      They are now <%= distance %> moves from the bait.
    <% end %>
    """
  end

  def render_action(assigns, action) do
    "did #{action.action_type}"
  end

  def render(assigns) do
    ~L"""
      <div>
        <%= if @action.role == :hunter do %>
          <%= live_component(@socket, PlayerRoleComponent,
            player: @hunter_player,
            role: :hunter,
          ) %>
        <% end %>
        <%= if @action.role == :monster do %>
          <%= live_component(@socket, PlayerRoleComponent,
            player: @monster_player,
            role: :monster,
          ) %>
        <% end %>
        <%= render_action(assigns, @action) %>
      </div>
    """
  end

end
