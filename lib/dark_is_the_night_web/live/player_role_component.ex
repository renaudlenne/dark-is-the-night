defmodule DarkIsTheNightWeb.PlayerRoleComponent do
  @moduledoc """
  Displays a player's name and role
  """

  use DarkIsTheNightWeb, :live_component

  def render(assigns) do
    ~L"""
      <span class="player <%= @role %>">
        <%= @player.name %>
        <i class="role"></i>
      </span>
    """
  end

end
