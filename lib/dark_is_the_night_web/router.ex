defmodule DarkIsTheNightWeb.Router do
  use DarkIsTheNightWeb, :router

  import Plug.BasicAuth
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {DarkIsTheNightWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :admin do
    plug :basic_auth,
         username: Application.get_env(:dark_is_the_night, :admin_username),
         password: Application.get_env(:dark_is_the_night, :admin_password)
  end

  scope "/", DarkIsTheNightWeb do
    pipe_through :browser

    live("/", LobbyLive)
    get("/join_game", GameController, :join)
    get("/leave", GameController, :leave)
    get("/game", GameController, :index)
  end

  scope "/admin" do
    pipe_through [:browser, :admin]
    live_dashboard "/dashboard", metrics: DarkIsTheNightWeb.Telemetry
  end
end
