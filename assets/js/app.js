// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "tailwindcss/tailwind.css"
import 'notyf/notyf.min.css'
import "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html"
import Clipboard from 'clipboard';
import {Socket} from "phoenix"
import NProgress from "nprogress"
import {LiveSocket} from "phoenix_live_view"
import { Notyf } from 'notyf';

const notyf = new Notyf({
  duration: 3000,
  position: {
    x: 'right',
    y: 'top',
  },
});

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")

const Hooks = {
  GameContainer: {
    mounted() {
      notyf.success('Joined game!');
    },
  },
};

const clipboard = new Clipboard('#copy-link', {
  text: () => {
    notyf.success('Copied link!');
    return window.location;
  },
});

clipboard.on('success', e => {
  console.log(e.trigger.innerHTML);
  e.trigger.innerHTML = 'Copied!';
  e.trigger.setAttribute('disabled', 'disabled');
  setTimeout(() => {
    e.trigger.innerHTML = 'Copy Link';
    e.trigger.removeAttribute('disabled');
  }, 1000);
});

let liveSocket = new LiveSocket("/live", Socket, {params: {_csrf_token: csrfToken}, hooks: Hooks})

// Show progress bar on live navigation and form submits
window.addEventListener("phx:page-loading-start", info => NProgress.start())
window.addEventListener("phx:page-loading-stop", info => NProgress.done())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket
